function isPalindrome(str) {
    let cleanStr = str.toLowerCase().replace(/[^a-zA-Z]/g, "");
    let reversedStr = cleanStr.split("").reverse().join("");
    return cleanStr === reversedStr;
}


function checkStringLength(str,maxLength) {
    return str.length <= maxLength;
}

let example1 = checkStringLength('checked string', 20);

console.log(example1);

let example2 = checkStringLength('checked string', 10);

 console.log(example2); 



 function calculateAge() {    
 let birthDate = prompt("Введіть дату народження у форматі 'рік-місяць-день' (наприклад, 1990-01-01):");
 let dob = new Date(birthDate);
 let currentDate = new Date();
 let ageDifference = currentDate - dob;
 let ageInYears = Math.floor(ageDifference / (1000 * 60 * 60 * 24 * 365.25));          return ageInYears; }
 let userAge = calculateAge(); console.log("Користувачу " + userAge + " років."); 